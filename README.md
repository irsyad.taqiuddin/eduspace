# Eduspace

**Languages and Tools:** 
![Javascript](https://img.shields.io/badge/-Javascript-black?logo=javascript&style=social)&nbsp;&nbsp;
![Django](https://img.shields.io/badge/-Django-black?logo=Django&style=social)&nbsp;&nbsp;
![Python](https://img.shields.io/badge/-Python-black?logo=Python&style=social)&nbsp;&nbsp;
![HTML5](https://img.shields.io/badge/-HTML5-black?logo=html5&style=social)&nbsp;&nbsp;
![CSS3](https://img.shields.io/badge/-CSS3-black?logo=css3&style=social)&nbsp;&nbsp;
![jQuery](https://img.shields.io/badge/-jQuery-black?logo=jquery&style=social)&nbsp;&nbsp;
![Bootstrap](https://img.shields.io/badge/-Bootstrap-black?logo=bootstrap&style=social)&nbsp;&nbsp;
![MySQL](https://img.shields.io/badge/-sqlite-black?logo=mysql&style=social)&nbsp;&nbsp;
![Git](https://img.shields.io/badge/-Git-black?logo=git&style=social)&nbsp;&nbsp;
![Gitlab](https://img.shields.io/badge/-Gitlab-black?logo=gitlab&style=social)&nbsp;&nbsp;
![Docker](https://img.shields.io/badge/-Docker-black?logo=docker&style=social)&nbsp;&nbsp;
![Docker](https://img.shields.io/docker/cloud/automated/mbiskho/mbiskho?style=social)&nbsp;&nbsp;


[![pipeline status](https://gitlab.com/muhammadazishusein/pewlicious/badges/master/pipeline.svg)](https://gitlab.com/muhammadazishusein/pewlicious/commits/master)

[![coverage report](https://gitlab.com/muhammadazishusein/pewlicious/badges/master/coverage.svg)](https://gitlab.com/muhammadazishusein/pewlicious/commits/master)



## _Education for everybody_
Eduspace is an app which have smilarities with udemy, coursera, and edx, its provide education for every people, age, and nation. Due to Covid 19 , we working on productivity app that impact on education during WFH. Segment market of this app is a person with range 10 - 25 years old, especially a person who currently pursuing degree in school or college level. 


> Not everyone has education
Not everybody has abillities
but everyone should have equal opprtunity for education
By 
-- John F Kenedy---


## Features

- Quiz live similar to Kahoot and mentimeter
- Login with sso ui and google email
- Live rangking for everyuser
- Reliable source of material
- System activation and verification
- System similar to Coursera & Edx


## Team

Berikut anggota kelompok kami:

- Bisma Khomeini  2006507600
- Zefanya Soplantila - 2006597696
- Siti Nurazizah Jamilah Rahat - 2006597411
- Fransiskus David - 2006597664
- Widya Ayu Trinita - 2006597550
- Irsyad Taqiuddin - 2006597821
- Pavita Maheswari I. P - s2006597393

LIVE APP : https://eduspce.herokuapp.com/ 

Team Management : https://trello.com/b/DV6yYkow/pbp-eduscpae

Design : soon...


## Installation

* Clone this repository (with HTTPS preferred)
    ```bash
    $ git clone https://gitlab.com/g1767/eduspace.git
    ```
* Activate virtualenv, or create one if none has been created  
    ```bash
    $ virtualenv env
    ```
* Install required packages  
    ```bash
    $ pip install -r requirements.txt
    ```
* Migrate if needed  
    ```bash
    $ python manage.py migrate
    ```
* CollectStatic if needed  
    ```bash
    $ python manage.py collectstatic
    ```
* Run the server in your local (`localhost:8000`)  
    ```bash
    $ python manage.py runserver
    ```

## Development

* Create a new branch from `master` with:  
    ```bash
    $ git checkout -b <your_name/scope>
    ```
    * example:
        * `bambang/admin`

* Do your changes, then push to remote repository to be merged  
    ```bash
    $ git add .
    $ git commit -m "<tag>(<scope>): <description>"
    $ git push origin <your branch>
    ```
    * examples of a *good* commit message:
        * `feature(admin): Implemented admin model`  
        * `fix(auth): Fix logging in not returning token`
        * `refactor(vote): Optimize searching`

* Submit merge request on the remote repository, wait for approvals, then merge if approved. You don't have to squash/delete the source branch after merge.
* After merge:
    ```bash
    $ git checkout master (or the target branch on the merge request)
    $ git pull origin master
    ```
* **Repeat**



## STACK

Eduspace is currently extended with the following plugins & stacks.
Instructions on how to use them in your own application are linked below.

| Plugin | README |
| ------ | ------ |
| Bootstrap | [https://getbootstrap.com/][PlDb] |
| Django | [https://www.djangoproject.com/][PlGh] |
| Jquery | [https://getbootstrap.com/][PlGd] |
| Animate.css | [https://jquery.com/][PlOd] |
| Google Analytics | [plugins/googleanalytics/README.md][PlGa] |


## Docker

Eduspace is very easy to install and deploy in a Docker container.

By default, the Docker will expose port 8000, so change this within the
Dockerfile if necessary. When ready, simply use the Dockerfile to
build the image.

```sh
cd Eduspace
docker build -t <youruser>/dillinger:${package.json.version} .
```

This will create the dillinger image and pull in the necessary dependencies.
Be sure to swap out `${package.json.version}` with the actual
version of Dillinger.

Once done, run the Docker image and map the port to whatever you wish on
your host. In this example, we simply map port 8000 of the host to
port 8080 of the Docker (or whatever port was exposed in the Dockerfile):

```sh
docker run -d -p 8000:8080 --restart=always --cap-add=SYS_ADMIN --name=dillinger <youruser>/dillinger:${package.json.version}
```

> Note: `--capt-add=SYS-ADMIN` is required for PDF rendering.

Verify the deployment by navigating to your server address in
your preferred browser.

```sh
127.0.0.1:8000
```

## License
[![License: Artistic-2.0](https://img.shields.io/badge/License-Perl-0298c3.svg)](https://opensource.org/licenses/Artistic-2.0)<br/>
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)<br/>
[![License](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg)](https://opensource.org/licenses/Apache-2.0) <br/>
[![License: ODbL](https://img.shields.io/badge/License-ODbL-brightgreen.svg)](https://opendatacommons.org/licenses/odbl/) <br/>
**Free Software :)*

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
